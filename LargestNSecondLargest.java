package lab1;
import java.util.*;
public class LargestNSecondLargest {

	public static void main(String[] args) {
		int size;
		int firstNum=0,secondNum=0;
		
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Number of Elements: ");
		size=sc.nextInt();
		int arr[]=new int[size];
		System.out.println("Enter the array Elements:");
		for(int i=0;i<size;i++)
		{
			arr[i]=sc.nextInt();
			if(firstNum<arr[i])
			{
				secondNum=firstNum;
				firstNum=arr[i];
			}
			else if(secondNum<arr[i])
			{
				secondNum=arr[i];
			}
		}
		System.out.println("The Largest Number is :"+firstNum);
		System.out.println("The Second Largest Number is :"+secondNum);
		
	}

}
