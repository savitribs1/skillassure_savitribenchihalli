package lab1;
import java.util.*;  
public class BinarySearch {

	public static void main(String[] args) {
		
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter num of elements:");
		int n=sc.nextInt();
		System.out.println("Enter "+n+ " integers:");
		int arr[]=new int[n];
		for ( int i=0;i<n;i++)
		{
			arr[i]=sc.nextInt();
		}
		System.out.println("Enter Search Value:");
		int k=sc.nextInt();
		int l=0,h=arr.length-1,index= -1;
		while(l<=h)
		{
			int m=(l+h)/2;
			if(arr[m]<k)
				l=m+1;
			else if(arr[m]>k)
				h=m-1;
			else
			{
				index=m;
				break;
			}
		}
		if(index == -1)
		{
			System.out.println("Search element is not found");
		}
		else
		{
			System.out.println(k+" found at location "+index);
		}
		
				
	}

}
