package lab1;
import java.util.*; 
public class AverageOfNumber {

	public static void main(String[] args) {
		int sum=0;
		double avg=0;
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the number:");
		int num=sc.nextInt();
		System.out.println("Enter "+num+ " integers:");
		int arr[]=new int[num];
		//int length=arr.length;
		for ( int i=0;i<num;i++)
		{
			arr[i]=sc.nextInt();
			sum+=arr[i];
		}
		avg=sum/num;
		System.out.println("Average of an Array:"+avg);
	}

}
