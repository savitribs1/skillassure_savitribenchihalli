package lab1;
import java.util.*; 
public class ArraySum {

	public static void main(String[] args) {
		int sum=0;
		
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter the number:");
		int num=sc.nextInt();
		System.out.println("Enter "+num+ " integers:");
		int arr[]=new int[num];
		
		for ( int i=0;i<num;i++)
		{
			arr[i]=sc.nextInt();
			sum+=arr[i];
		}
		
		System.out.println("Sum of an Array:"+sum);
	}

}
