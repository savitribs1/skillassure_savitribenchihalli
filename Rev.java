public class Rev
{
	public static void main(String[] args) {
		int num=3251;
		int rev=0;
		while(num>0)
		{
		    rev=rev+num%10;
		    num=num/10;
		}
	System.out.print("Reverse:" +rev);
	}
}

/*OUTPUT: Reverse:11
step1:while(3251>0)//it is true so
rev=0+3251%10;//0+1=1
num=3251/10;//325

step2: while(325>0)//it is true so
rev=1+325%10;//1+5=6
num=325/10;//32

step3: while(32>0)//it is true so
rev=6+32%10;//6+2=8
num=32/10;//3

step4: while(3>0)//it is true so
rev=8+3%10;//3+8=11
num=3/10;//0

step5: while(0>0)//it is false so exit from the loop and dispaly the output*/





