package lab1;
import java.util.*;
public class SwapTwoNum {

	public static void main(String[] args) {
		System.out.println("Enter Two Number:");
		Scanner sc=new Scanner(System.in);
		int num1=sc.nextInt();
		int num2=sc.nextInt();
		System.out.print("Before Swapping Two Numbers: "+num1);
		System.out.print(" ");
		System.out.println(num2);
		int temp;
		temp=num1;
		num1=num2;
		num2=temp;
		System.out.print("After Swapping Two Numbers: "+num1);
		System.out.print(" ");
		System.out.println(num2);
	}

}
